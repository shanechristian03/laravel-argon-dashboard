<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $products = Product::where('active', true)->simplePaginate(10);
            return view('pages.products.index', ['products' => $products]);
        }
        catch (\Throwable $th) {
            return view('pages.products.index')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.products.save');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:100',
            'price' => 'required|numeric|between:1000,99999999',
            'stock' => 'required|numeric|between:0,99999999',
        ]);

        try {
            $product = new Product();
            $product->name = $validated['name'];
            $product->price = $validated['price'];
            $product->stock = $validated['stock'];

            $product->save();
            return redirect()->route('products')->with('success', 'New product has been added!');
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = Product::find($id);
        if ($product == null) {
            return back()->with('error', 'An error has occured! Please try again!');
        }

        return view('pages.products.save', ['update' => true, 'product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validated = $request->validate([
            'name' => 'required|max:100',
            'price' => 'required|numeric|between:1000,99999999',
            'stock' => 'required|numeric|between:0,99999999',
        ]);

        try {
            $product = Product::find($id);
            if ($product == null) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $product->name = $validated['name'];
            $product->price = $validated['price'];
            $product->stock = $validated['stock'];

            $product->save();
            return redirect()->route('products')->with('success', "Product (ID: $id) has been updated!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    public function deactivate(string $id)
    {
        try {
            $product = Product::find($id);
            if ($product == null) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $product->active = false;
            $product->save();
            return redirect()->route('products')->with('success', "Product (ID: $id) has been deleted!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    public function getProductDetails(string $id)
    {
        try {
            $product = Product::find($id);
            if ($product == null) {
                return response()->json([], 404);
            }
            return response()->json($product);
        }
        catch (\Throwable $th) {
            return response()->json(['msg' => 'An error has occured! Please try again!'], 500);
        }
    }
}

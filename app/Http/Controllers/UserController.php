<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function showProfile()
    {
        return view('pages.users.profile');
    }

    public function updateProfile(Request $request)
    {
        $attributes = $request->validate([
            'username' => ['required','max:255', 'min:2'],
            'name' => ['max:100'],
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore(auth()->user()->id)],
            'address' => ['max:100'],
            'city' => ['max:100'],
            'country' => ['max:100'],
            'postal' => ['max:100'],
        ]);

        auth()->user()->update([
            'username' => $attributes['username'],
            'name' => $attributes['name'],
            'email' => $attributes['email'],
            'address' => $attributes['address'],
            'city' => $attributes['city'],
            'country' => $attributes['country'],
            'postal' => $attributes['postal'],
        ]);
        return back()->with('success', 'Profile succesfully updated!');
    }

    public function viewUser(string $id)
    {
        try {
            $user = User::find($id);
            return view('pages.users.user-profile', ['user' => $user]);
        }
        catch (\Throwable $th) {
            return view('pages.users.user-profile')->with('error', 'An error has occured! Please try again!');
        }
    }

    public function viewAllUsers()
    {
        try {
            $users = User::simplePaginate(10);
            return view('pages.users.user-management', ['users' => $users]);
        }
        catch (\Throwable $th) {
            return view('pages.users.user-management')->with('error', 'An error has occured! Please try again!');
        }
    }

    public function activate(string $id)
    {
        try {
            $user = User::find($id);
            $user->active = true;
            $user->save();
            return back()->with('success', "$user->username has been activated.");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    public function deactivate(string $id)
    {
        try {
            $user = User::find($id);
            $user->active = false;
            $user->save();
            return back()->with('success', "$user->username has been deactivated.");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }
}

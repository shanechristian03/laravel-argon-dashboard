<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Product;
use App\Models\SalesMaster;
use App\Models\SalesDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $sales = SalesMaster::with('customer')->simplePaginate(10);
            return view('pages.sales.index', ['sales' => $sales]);
        }
        catch (\Throwable $th) {
            return view('pages.sales.index')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $sale = SalesMaster::with('details', 'customer')->find($id);
            if (!$sale) {
                return view('pages.sales.show')->with('error', 'An error has occured! Please try again!');
            }

            return view('pages.sales.show', ['sale' => $sale]);
        }
        catch (\Throwable $th) {
            return view('pages.sales.show')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        try {
            $products = Product::all();
            $customers = Customer::all();
            return view('pages.sales.create', ['products' => $products, 'customers' => $customers]);
        }
        catch (\Throwable $th) {
            return view('pages.sales.create')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $salesMaster = new SalesMaster();
            $salesMaster->customer_id = $request->input('customer_id');
            $salesMaster->sale_date = $request->input('sale_date');
            $salesMaster->discount = $request->input('discount');
            $salesMaster->tax = $request->input('tax');
            $salesMaster->total = $request->input('total');
            $salesMaster->save();

            foreach ($request->input('details') as $detail) {
                $salesDetail = new SalesDetail();
                $salesDetail->master_id = $salesMaster->id;
                $salesDetail->product_id = $detail['product_id'];
                $salesDetail->price = $detail['price'];
                $salesDetail->qty = $detail['qty'];
                $salesDetail->discount = $detail['discount'];
                $salesDetail->subtotal = $detail['subtotal'];
                $salesDetail->save();
            }
        }
        catch (\Throwable $th) {
            //
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $sale = SalesMaster::with('details', 'customer')->find($id);
        if (!$sale) {
            return back()->with('error', 'An error has occured! Please try again!');
        }

        $products = Product::all();
        $customers = Customer::all();
        return view('pages.sales.update', ['sale' => $sale, 'products' => $products, 'customers' => $customers]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $salesMaster = SalesMaster::find($id);
            $salesMaster->customer_id = $request->input('customer_id');
            $salesMaster->sale_date = $request->input('sale_date');
            $salesMaster->discount = $request->input('discount');
            $salesMaster->tax = $request->input('tax');
            $salesMaster->total = $request->input('total');
            $salesMaster->save();

            SalesDetail::where('master_id', $salesMaster->id)->delete();

            foreach ($request->input('details') as $detail) {
                $salesDetail = new SalesDetail();
                $salesDetail->master_id = $salesMaster->id;
                $salesDetail->product_id = $detail['product_id'];
                $salesDetail->price = $detail['price'];
                $salesDetail->qty = $detail['qty'];
                $salesDetail->discount = $detail['discount'];
                $salesDetail->subtotal = $detail['subtotal'];
                $salesDetail->save();
            }
        }
        catch (\Throwable $th) {
            //
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $sale = SalesMaster::find($id);
            if ($sale == null) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $sale->delete();
            return redirect()->route('sales')->with('success', "Sale (ID: $id) has been deleted!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }
}

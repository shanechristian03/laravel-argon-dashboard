<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\SalesMaster;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $deliveries = Delivery::with('sale.customer')->simplePaginate(10);
            return view('pages.deliveries.index', ['deliveries' => $deliveries]);
        }
        catch (\Throwable $th) {
            return view('pages.deliveries.index')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $delivery = Delivery::with('sale.customer')->find($id);
            if (!$delivery) {
                return view('pages.deliveries.show')->with('error', 'An error has occured! Please try again!');
            }

            return view('pages.deliveries.show', ['delivery' => $delivery]);
        }
        catch (\Throwable $th) {
            return view('pages.deliveries.show')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        try {
            $sales = SalesMaster::all();
            return view('pages.deliveries.save', ['sales' => $sales]);
        }
        catch (\Throwable $th) {
            return view('pages.deliveries.save')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $delivery = new Delivery();
            $delivery->sale_id = $request->sale_id;
            $delivery->order_received_date = empty($request->order_received_date) ? null : $request->order_received_date;
            $delivery->order_packing_date = empty($request->order_packing_date) ? null : $request->order_packing_date;
            $delivery->start_delivering_date = empty($request->start_delivering_date) ? null : $request->start_delivering_date;
            $delivery->product_received_date = empty($request->product_received_date) ? null : $request->product_received_date;
            $delivery->address = $request->address;
            $delivery->city = $request->city;
            $delivery->country = $request->country;
            $delivery->distance = $request->distance;

            $delivery->save();
            return redirect()->route('deliveries')->with('success', 'New delivery has been added!');
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        try {
            $delivery = Delivery::find($id);
            if (!$delivery) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $sales = SalesMaster::all();
            return view('pages.deliveries.save', ['update' => true, 'sales' => $sales, 'delivery' => $delivery]);
        }
        catch (\Throwable $th) {
            return view('pages.deliveries.save')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $delivery = Delivery::find($id);
            if (!$delivery) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $delivery->sale_id = $request->sale_id;
            $delivery->order_received_date = empty($request->order_received_date) ? null : $request->order_received_date;
            $delivery->order_packing_date = empty($request->order_packing_date) ? null : $request->order_packing_date;
            $delivery->start_delivering_date = empty($request->start_delivering_date) ? null : $request->start_delivering_date;
            $delivery->product_received_date = empty($request->product_received_date) ? null : $request->product_received_date;
            $delivery->address = $request->address;
            $delivery->city = $request->city;
            $delivery->country = $request->country;
            $delivery->distance = $request->distance;

            $delivery->save();
            return redirect()->route('deliveries')->with('success', "Delivery ID $id has been updated!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $delivery = Delivery::find($id);
            if (!$delivery) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $delivery->delete();
            return redirect()->route('deliveries')->with('success', "Delivery ID $id has been deleted!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $customers = Customer::where('active', true)->simplePaginate(10);
            return view('pages.customers.index', ['customers' => $customers]);
        }
        catch (\Throwable $th) {
            return view('pages.customers.index')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $customer = Customer::find($id);
            if ($customer == null) {
                return view('pages.customers.show')->with('error', 'An error has occured! Please try again!');
            }

            return view('pages.customers.show', ['customer' => $customer]);
        }
        catch (\Throwable $th) {
            return view('pages.customers.show')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.customers.save');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:100',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:20',
            'company_name' => 'max:100',
            'company_address' => 'max:255',
            'company_city' => 'max:50',
            'company_country' => 'max:50',
            'company_zip_code' => 'max:15',
            'company_website' => 'max:255',
        ]);

        try {
            $customer = new Customer();
            $customer->name = $validated['name'];
            $customer->email = $validated['email'];
            $customer->phone = $validated['phone'];
            $customer->company_name = $validated['company_name'];
            $customer->company_address = $validated['company_address'];
            $customer->company_city = $validated['company_city'];
            $customer->company_country = $validated['company_country'];
            $customer->company_zip_code = $validated['company_zip_code'];
            $customer->company_website = $validated['company_website'];

            $customer->save();
            return redirect()->route('customers')->with('success', 'New customer has been added!');
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $customer = Customer::find($id);
        if ($customer == null) {
            return back()->with('error', 'An error has occured! Please try again!');
        }

        return view('pages.customers.save', ['update' => true, 'customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validated = $request->validate([
            'name' => 'required|max:100',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:20',
            'company_name' => 'max:100',
            'company_address' => 'max:255',
            'company_city' => 'max:50',
            'company_country' => 'max:50',
            'company_zip_code' => 'max:15',
            'company_website' => 'max:255',
        ]);

        try {
            $customer = Customer::find($id);
            if ($customer == null) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $customer->name = $validated['name'];
            $customer->email = $validated['email'];
            $customer->phone = $validated['phone'];
            $customer->company_name = $validated['company_name'];
            $customer->company_address = $validated['company_address'];
            $customer->company_city = $validated['company_city'];
            $customer->company_country = $validated['company_country'];
            $customer->company_zip_code = $validated['company_zip_code'];
            $customer->company_website = $validated['company_website'];

            $customer->save();
            return redirect()->route('customers')->with('success', "Customer (ID: $id) has been updated!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    public function deactivate(string $id)
    {
        try {
            $customer = Customer::find($id);
            if ($customer == null) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $customer->active = false;
            $customer->save();
            return redirect()->route('customers')->with('success', "Customer (ID: $id) has been deleted!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    public function getCustomerDetails(string $id)
    {
        try {
            $customer = Customer::find($id);
            if ($customer == null) {
                return response()->json([], 404);
            }
            return response()->json($customer);
        }
        catch (\Throwable $th) {
            return response()->json(['msg' => 'An error has occured! Please try again!'], 500);
        }
    }
}

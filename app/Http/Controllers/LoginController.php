<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Password;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /**
     * Display login page.
     *
     * @return Renderable
     */
    public function show()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        $remember = false;

        if ($request->has('remember')) {
            $remember = true;
        }

        $user = User::where('email', $credentials['email'])->first();
        if (isset($user->google_id) || !$user->active) {
            return back()->withErrors([
                'email' => 'The provided credentials do not match our records.',
            ]);
        }

        if (Auth::attempt(['email' => $credentials['email'], 'password' => $credentials['password']], $remember)) {
            $request->session()->regenerate();

            return redirect()->intended('dashboard');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function google()
    {
        return Socialite::driver('google')->redirect();
    }

    public function redirectFromGoogle()
    {
        try {
            $googleUser = Socialite::driver('google')->user();
            $user = User::where('google_id', $googleUser->getId())->first();

            if ($user) {
                if ($user->active) {
                    Auth::login($user, true);
                }
                return redirect()->route('home');
            }
            else {
                $newUser = User::create([
                    'username' => $googleUser->getName(),
                    'name' => $googleUser->getName(),
                    'email' => $googleUser->getEmail(),
                    'password' => bcrypt('google'),
                    'google_id' => $googleUser->getId()
                ]);

                Auth::login($newUser, true);
                return redirect()->route('home');
            }
        }
        catch (\Throwable $th) {
            return redirect('login')->withErrors([
                'msg' => 'An error has occured! Please try again!'
            ]);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login');
    }
}

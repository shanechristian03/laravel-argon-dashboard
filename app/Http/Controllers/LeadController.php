<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Lead;
use Illuminate\Http\Request;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $leads = Lead::simplePaginate(10);
            return view('pages.leads.index', ['leads' => $leads]);
        }
        catch (\Throwable $th) {
            return view('pages.leads.index')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $lead = Lead::find($id);
            if ($lead == null) {
                return view('pages.leads.show')->with('error', 'An error has occured! Please try again!');
            }

            return view('pages.leads.show', ['lead' => $lead]);
        }
        catch (\Throwable $th) {
            return view('pages.leads.show')->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.leads.save');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:100',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:20',
            'company_name' => 'max:100',
            'company_address' => 'max:255',
            'company_city' => 'max:50',
            'company_country' => 'max:50',
            'company_zip_code' => 'max:15',
            'company_website' => 'max:255',
        ]);

        try {
            $lead = new Lead();
            $lead->name = $validated['name'];
            $lead->email = $validated['email'];
            $lead->phone = $validated['phone'];
            $lead->company_name = $validated['company_name'];
            $lead->company_address = $validated['company_address'];
            $lead->company_city = $validated['company_city'];
            $lead->company_country = $validated['company_country'];
            $lead->company_zip_code = $validated['company_zip_code'];
            $lead->company_website = $validated['company_website'];

            $lead->save();
            return redirect()->route('leads')->with('success', 'New lead has been added!');
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $lead = Lead::find($id);
        if ($lead == null) {
            return back()->with('error', 'An error has occured! Please try again!');
        }

        return view('pages.leads.save', ['update' => true, 'lead' => $lead]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validated = $request->validate([
            'name' => 'required|max:100',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:20',
            'company_name' => 'max:100',
            'company_address' => 'max:255',
            'company_city' => 'max:50',
            'company_country' => 'max:50',
            'company_zip_code' => 'max:15',
            'company_website' => 'max:255',
        ]);

        try {
            $lead = Lead::find($id);
            if ($lead == null) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $lead->name = $validated['name'];
            $lead->email = $validated['email'];
            $lead->phone = $validated['phone'];
            $lead->company_name = $validated['company_name'];
            $lead->company_address = $validated['company_address'];
            $lead->company_city = $validated['company_city'];
            $lead->company_country = $validated['company_country'];
            $lead->company_zip_code = $validated['company_zip_code'];
            $lead->company_website = $validated['company_website'];

            $lead->save();
            return redirect()->route('leads')->with('success', "Lead (ID: $id) has been updated!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $lead = Lead::find($id);
            if ($lead == null) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $lead->delete();
            return redirect()->route('leads')->with('success', "Lead (ID: $id) has been deleted!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }

    public function convert(string $id)
    {
        try {
            $lead = Lead::find($id);
            if ($lead == null) {
                return back()->with('error', 'An error has occured! Please try again!');
            }

            $customer = new Customer();
            $customer->name = $lead->name;
            $customer->email = $lead->email;
            $customer->phone = $lead->phone;
            $customer->company_name = $lead->company_name;
            $customer->company_address = $lead->company_address;
            $customer->company_city = $lead->company_city;
            $customer->company_country = $lead->company_country;
            $customer->company_zip_code = $lead->company_zip_code;
            $customer->company_website = $lead->company_website;

            $lead->delete();
            $customer->save();
            return redirect()->route('customers')->with('success', "Lead (ID: $id) has been converted as a new customer!");
        }
        catch (\Throwable $th) {
            return back()->with('error', 'An error has occured! Please try again!');
        }
    }
}

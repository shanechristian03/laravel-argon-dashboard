<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'sale_id',
        'order_received_date',
        'order_packing_date',
        'start_delivering_date',
        'product_received_date',
        'address',
        'city',
        'country',
        'distance',
    ];

    public function getDistanceInKm()
    {
        return $this->distance / 1000 . ' km';
    }

    public function sale()
    {
        return $this->belongsTo(SalesMaster::class, 'sale_id', 'id');
    }
}

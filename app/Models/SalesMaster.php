<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesMaster extends Model
{
    use HasFactory;

    protected $table = 'sales_master';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'customer_id',
        'sale_date',
        'discount',
        'tax',
        'total'
    ];

    public function getSaleDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)
            ->timezone('Asia/Jakarta')
            ->toDateString();
    }

    public function getFormattedPrice($value)
    {
        return 'Rp ' . number_format($value, 2, ',', '.');
    }

    public function details()
    {
        return $this->hasMany(SalesDetail::class, 'master_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesDetail extends Model
{
    use HasFactory;

    protected $table = 'sales_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'master_id',
        'product_id',
        'price',
        'qty',
        'discount',
        'subtotal'
    ];

    public function getFormattedPrice($value)
    {
        return 'Rp ' . number_format($value, 2, ',', '.');
    }

    public function master()
    {
        return $this->belongsTo(SalesMaster::class, 'master_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}

<aside class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 pb-3"
    id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
            aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="{{ route('home') }}"
            target="_blank">
            <img src="/img/logo-ct-dark.png" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold">Argon Dashboard</span>
        </a>
    </div>
    <hr class="horizontal dark mt-0 mb-2">
    <div id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item mt-4 d-flex align-items-center">
                <div class="ps-4">
                    <i class="fa fa-shopping-cart" style="color: #f4645f;"></i>
                </div>
                <h6 class="ms-2 text-uppercase text-xs font-weight-bolder opacity-6 mb-0">Sales & Deliveries</h6>
            </li>
            <li class="nav-item">
                <a class="nav-link py-2 {{ Route::currentRouteName() == 'products' ? 'active' : '' }}" href="{{ route('products') }}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-app text-dark text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Product Management</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-2 {{ Route::currentRouteName() == 'sales' ? 'active' : '' }}" href="{{ route('sales') }}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-money-coins text-dark text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Sales</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-2 {{ str_contains(request()->url(), 'deliveries') == true ? 'active' : '' }}" href="{{ route('deliveries') }}">
                    <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-delivery-fast text-dark text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Deliveries</span>
                </a>
            </li>
            <li class="nav-item mt-4 d-flex align-items-center">
                <div class="ps-4">
                    <i class="fa fa-group" style="color: #f4645f;"></i>
                </div>
                <h6 class="ms-2 text-uppercase text-xs font-weight-bolder opacity-6 mb-0">Leads & Customers</h6>
            </li>
            <li class="nav-item">
                <a class="nav-link py-2 {{ Route::currentRouteName() == 'leads' ? 'active' : '' }}" href="{{ route('leads') }}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-book-bookmark text-dark text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Lead Management</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-2 {{ str_contains(request()->url(), 'customers') == true ? 'active' : '' }}" href="{{ route('customers') }}">
                    <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-book-bookmark text-dark text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Customer Management</span>
                </a>
            </li>
            <li class="nav-item mt-4 d-flex align-items-center">
                <div class="ps-4">
                    <i class="fa fa-address-card" style="color: #f4645f;"></i>
                </div>
                <h6 class="ms-2 text-uppercase text-xs font-weight-bolder opacity-6 mb-0">Users / Accounts</h6>
            </li>
            <li class="nav-item">
                <a class="nav-link py-2 {{ Route::currentRouteName() == 'profile' ? 'active' : '' }}" href="{{ route('profile') }}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-badge text-dark text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Profile</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-2 {{ str_contains(request()->url(), 'user-management') == true ? 'active' : '' }}" href="{{ route('user-management') }}">
                    <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-single-02 text-dark text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">User Management</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
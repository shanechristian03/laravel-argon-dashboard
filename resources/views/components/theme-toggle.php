<div>
	<input type="checkbox" class="theme-toggle" id="theme-toggle" />
	<label class="theme-label mb-0" for="theme-toggle" onclick="darkMode(this)">
		<i class="fas fa-moon me-1"></i>
		<i class="fas fa-sun"></i>
		<div class="ball"></div>
	</label>
</div>
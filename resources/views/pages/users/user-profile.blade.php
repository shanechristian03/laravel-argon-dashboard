@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => $user->username . "'s Profile"])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="container-fluid pt-0 pb-4">
        <div class="row justify-content-center">

            @if (!session()->has('error'))
                <div class="col-md-8 mb-5">
                    <div class="card card-profile">
                        <div class="card-header d-flex flex-column align-items-center pt-4 pb-0">
                            <div class="avatar avatar-xxl position-relative">
                                <img src="/img/user-profile.jpg" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
                            </div>
                            <h4 class="mt-2 mb-1">
                                {{ $user->name ?? 'Name' }}
                            </h4>
                        </div>
                        <div class="card-body">
                            <p class="text-uppercase text-sm">User Information</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Username</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="username" value="{{ $user->username }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Email address</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="email" name="email" value="{{ $user->email }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <hr class="horizontal dark">
                            <p class="text-uppercase text-sm">Contact Information</p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Address</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="address" value="{{ $user->address ?? '-' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">City</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="city" value="{{ $user->city ?? '-' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Country</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="country" value="{{ $user->country ?? '-' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Postal code</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="postal" value="{{ $user->postal ?? '-' }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            
        @include('layouts.footers.auth.footer')
    </div>
@endsection

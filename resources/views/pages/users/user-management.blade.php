@extends('layouts.app')

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'User Management'])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="row mt-4 mb-6 mx-4">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h4>Users</h4>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0 pt-2 table-paginate">
                        <table class="table align-items-center mb-0 mt-2">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Email</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Created At</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr onclick="window.location = '{{ route('profile.view', ['id' => $user->id]) }}';" style="cursor: pointer;">
                                        <td>
                                            <div class="d-flex px-3 py-1">
                                                <div>
                                                    <img src="/img/user-profile.jpg" class="avatar me-3" alt="image">
                                                </div>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $user->name }}</h6>
                                                    <p class="mb-0 text-sm">{{ $user->username }}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{ $user->email }}</p>
                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            <p class="text-sm font-weight-bold mb-0">{{ $user->created_at }}</p>
                                        </td>
                                        @if (auth()->user()->can_manage_users && auth()->user()->id != $user->id)
                                            <td class="align-middle text-center">
                                                <div class="d-flex py-1 justify-content-center align-items-center">
                                                    @if ($user->active == true)
                                                        <form role="form" method="post" action="{{ route('user.deactivate', ['id' => $user->id]) }}" id="deactivate-form-{{ $user->id }}">
                                                            @method('PATCH')
                                                            @csrf
                                                            <a class="btn btn-danger text-sm font-weight-bold mb-0 px-2"
                                                                href="{{ route('user.deactivate', ['id' => $user->id]) }}"
                                                                onclick="event.preventDefault(); event.stopPropagation(); document.getElementById('deactivate-form-{{ $user->id }}').submit();">
                                                                <i class="fa fa-ban"></i>
                                                                <span class="ms-1">Deactivate</span>
                                                            </a>
                                                        </form>
                                                    @else
                                                        <form role="form" method="post" action="{{ route('user.activate', ['id' => $user->id]) }}" id="activate-form-{{ $user->id }}">
                                                            @method('PATCH')
                                                            @csrf
                                                            <a class="btn btn-success text-sm font-weight-bold mb-0 px-2"
                                                                href="{{ route('user.activate', ['id' => $user->id]) }}"
                                                                onclick="event.preventDefault(); event.stopPropagation(); document.getElementById('activate-form-{{ $user->id }}').submit();">
                                                                <i class="fa fa-arrow-up"></i>
                                                                <span class="ms-1">Activate</span>
                                                            </a>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                        @else
                                            <td class="align-middle text-center">-</td>
                                        @endif
                                    </tr>
                                @endforeach
                                {{ $users->links() }}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

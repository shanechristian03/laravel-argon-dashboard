@extends('layouts.app')

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'Customer Management'])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="row mt-4 mb-6 mx-4">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between pb-0">
                    <h4>Customers</h4>
                    <a class="btn btn-primary font-weight-bold py-2 px-3" href="{{ route('customer.create') }}">
                        <i class="fa fa-plus"></i>
                        <span>Add Customer</span>
                    </a>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0 pt-2 table-paginate">
                        <table class="table align-items-center mb-0 mt-2">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Email</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Phone</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Company</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($customers as $customer)
                                    <tr onclick="window.location = '{{ route('customer.show', ['id' => $customer->id]) }}';" style="cursor: pointer;">
                                        <td class="ps-4">
                                            <p class="text-sm font-weight-bold mb-0">{{ $customer->name }}</p>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{ $customer->email }}</p>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{ $customer->phone }}</p>
                                        </td>
                                        <td>
                                            <p class="text-sm text-center font-weight-bold mb-0">{{ $customer->company_name ?? '-' }}</p>
                                        </td>
                                        <td class="align-middle text-center py-1">
                                          <div class="d-flex py-1 justify-content-center align-items-center">
                                              <a class="btn btn-success font-weight-bold me-1 mb-0 p-2"
                                                  style="font-size: 0.75rem;"
                                                  href="{{ route('customer.edit', ['id' => $customer->id]) }}"
                                                  onclick="event.stopPropagation();">
                                                  <i class="fa fa-edit"></i>
                                                  <span>Edit</span>
                                              </a>
                                              <form role="form" method="post" action="{{ route('customer.delete', ['id' => $customer->id]) }}" id="delete-form-{{ $customer->id }}">
                                                  @method('PATCH')
                                                  @csrf
                                                  <a class="btn btn-danger font-weight-bold mb-0 p-2"
                                                      style="font-size: 0.75rem;"
                                                      href="{{ route('customer.delete', ['id' => $customer->id]) }}"
                                                      onclick="event.preventDefault(); event.stopPropagation(); document.getElementById('delete-form-{{ $customer->id }}').submit();">
                                                      <i class="fa fa-trash"></i>
                                                      <span>Delete</span>
                                                  </a>
                                              </form>
                                          </div>
                                      </td>
                                    </tr>
                                @endforeach
                                {{ $customers->links() }}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@php
    $update = $update ?? false;
@endphp

@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => $update ? 'Edit Delivery' : 'Add New Delivery'])
    <div id="alert">
        @include('components.alert')
    </div>

    <div class="container-fluid py-4">
        @if (!session()->has('error'))
            <div class="row justify-content-center">
                <div class="card my-4 w-75">
                    <div class="card-header pb-0 pt-4">
                        <h5>{{ $update ? "Edit Delivery ID $delivery->id" : 'Add New Delivery' }}</h5>
                    </div>
                    <div class="card-body py-4">
                        <form action="{{ $update ? route('delivery.update', ['id' => $delivery->id]) : route('delivery.store') }}" method="POST">
                            @method($update ? 'PATCH' : 'POST')
                            @csrf

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Sale ID</label>
                                        <select class="form-control ps-2 pt-1" type="text" name="sale_id">
                                            @foreach ($sales as $sale)
                                                <option value="{{ $sale->id }}" @selected($update && $delivery->sale_id == $sale->id)>
                                                    SALE #{{ $sale->id }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-md-8 mb-1">
                                    <h6 class="m-0 ms-2">The order from the customer was received at</h6>
                                </div>
                                <div class="col-md-4 mb-1">
                                    <input type="text" name="order_received_date" id="order_received_date"
                                        class="form-control ps-2 pt-1" value="{{ $update ? $delivery->order_received_date : '' }}">
                                </div>
    
                                <div class="col-md-8 mb-1">
                                    <h6 class="m-0 ms-2">The order packing was done at</h6>
                                </div>
                                <div class="col-md-4 mb-1">
                                    <input type="text" name="order_packing_date" id="order_packing_date"
                                        class="form-control ps-2 pt-1" value="{{ $update ? $delivery->order_packing_date : '' }}">
                                </div>
    
                                <div class="col-md-8 mb-1">
                                    <h6 class="m-0 ms-2">The delivery of the product started at</h6>
                                </div>
                                <div class="col-md-4 mb-1">
                                    <input type="text" name="start_delivering_date" id="start_delivering_date"
                                        class="form-control ps-2 pt-1" value="{{ $update ? $delivery->start_delivering_date : '' }}">
                                </div>
    
                                <div class="col-md-8 mb-1">
                                    <h6 class="m-0 ms-2">The customer received the product at</h6>
                                </div>
                                <div class="col-md-4 mb-1">
                                    <input type="text" name="product_received_date" id="product_received_date"
                                        class="form-control ps-2 pt-1" value="{{ $update ? $delivery->product_received_date : '' }}">
                                </div>
                            </div>
                            <hr class="horizontal dark">
                            <h6 class="text-uppercase text-sm">Destination</h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Address</label>
                                        <input class="form-control" type="text" name="address" value="{{ $update ? $delivery->address : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">City</label>
                                        <input class="form-control" type="text" name="city" value="{{ $update ? $delivery->city : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Country</label>
                                        <input class="form-control" type="text" name="country" value="{{ $update ? $delivery->country : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Estimated Distance (in meter)</label>
                                        <input class="form-control" type="text" name="distance" value="{{ $update ? $delivery->distance : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-7 align-self-end text-end mb-2">
                                    <button type="submit" class="btn btn-primary btn-sm ms-auto mb-2">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
        @include('layouts.footers.auth.footer')
    </div>

    <script>
        $(function () {
            $('#order_received_date').datepicker({ dateFormat: 'yy-mm-dd' })
            $('#order_packing_date').datepicker({ dateFormat: 'yy-mm-dd' })
            $('#start_delivering_date').datepicker({ dateFormat: 'yy-mm-dd' })
            $('#product_received_date').datepicker({ dateFormat: 'yy-mm-dd' })
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'Delivery Management'])
    <div id="alert">
        @include('components.alert')
    </div>

    @if (!session()->has('error'))
        <div class="row mt-4 mb-6 mx-4">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between pb-0">
                        <h4>Deliveries</h4>
                        <a class="btn btn-primary font-weight-bold py-2 px-3" href="{{ route('delivery.create') }}">
                            <i class="fa fa-plus"></i>
                            <span>Add Delivery</span>
                        </a>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0 pt-2 table-paginate">
                            <table class="table align-items-center mb-0 mt-2">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">ID</th>
                                        <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Sale ID</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Customer</th>
                                        <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Distance</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Delivered At</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($deliveries as $delivery)
                                        <tr onclick="window.location = '{{ route('delivery.show', ['id' => $delivery->id]) }}';" style="cursor: pointer;">
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0">{{ $delivery->id }}</p>
                                            </td>
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0">{{ $delivery->sale_id }}</p>
                                            </td>
                                            <td>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $delivery->sale->customer->name }}</h6>
                                                    @isset($delivery->sale->customer->company_name)
                                                        <p class="mb-0 text-sm">from&nbsp;{{ $delivery->sale->customer->company_name }}</p>
                                                    @endisset
                                                </div>
                                            </td>
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0">{{ $delivery->getDistanceInKm() }}</p>
                                            </td>
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0">{{ $delivery->product_received_date ?? '-' }}</p>
                                            </td>
                                            <td class="align-middle text-center py-1">
                                            <div class="d-flex py-1 justify-content-center align-items-center">
                                                <a class="btn btn-success font-weight-bold me-1 mb-0 p-2"
                                                    style="font-size: 0.75rem;"
                                                    href="{{ route('delivery.edit', ['id' => $delivery->id]) }}"
                                                    onclick="event.stopPropagation();">
                                                    <i class="fa fa-edit"></i>
                                                    <span>Edit</span>
                                                </a>
                                                <form role="form" method="post" action="{{ route('delivery.delete', ['id' => $delivery->id]) }}" id="delete-form-{{ $delivery->id }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a class="btn btn-danger font-weight-bold mb-0 p-2"
                                                        style="font-size: 0.75rem;"
                                                        href="{{ route('delivery.delete', ['id' => $delivery->id]) }}"
                                                        onclick="event.preventDefault(); event.stopPropagation(); document.getElementById('delete-form-{{ $delivery->id }}').submit();">
                                                        <i class="fa fa-trash"></i>
                                                        <span>Delete</span>
                                                    </a>
                                                </form>
                                            </div>
                                        </td>
                                        </tr>
                                    @endforeach
                                    {{ $deliveries->links() }}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

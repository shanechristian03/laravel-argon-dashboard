@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => "Delivery ID $delivery->id"])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="container-fluid py-4">
        @if (!session()->has('error'))
            <div class="row justify-content-center">
                <div class="card my-4 w-75">
                    <div class="card-header pb-0 pt-4">
                        <div class="row m-0 p-0 align-items-center justify-content-between">
                            <h5 class="col-md-8 p-0">Delivery (ID: {{ $delivery->id }}) Status</h5>
                            <div class="col-md-4 p-0 mb-2 text-end">
                                <a class="btn btn-primary font-weight-bold shadow-sm mb-0"
                                    href="{{ route('sale.show', ['id' => $delivery->sale_id]) }}">
                                    <span>Sale Details</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-4">
                        <div class="row align-items-center">
                            <div class="col-md-12 d-flex align-items-center my-2">
                                <div class="{{ isset($delivery->order_received_date) ? 'bg-success' : 'bg-dark' }} rounded-circle d-flex justify-content-center align-items-center me-2"
                                    style="width: 2rem; height: 2rem;">
                                    @isset($delivery->order_received_date) <i class="fa fa-check text-white p-0"></i> @endisset
                                </div>
                                <span class="text-dark">----------</span>
                                <h6 class="m-0 ms-2">
                                    The order from the customer was received at <span class="fw-bolder">{{ $delivery->order_received_date ?? '-' }}</span>
                                </h6>
                            </div>

                            <div class="col-md-12 d-flex align-items-center my-1">
                                <div class="bg-transparent d-flex justify-content-center align-items-center"
                                    style="width: 2rem; height: 2rem;">
                                    <i class="fa fa-arrow-down text-dark"></i>
                                </div>
                            </div>
                            
                            <div class="col-md-12 d-flex align-items-center my-2">
                                <div class="{{ isset($delivery->order_packing_date) ? 'bg-success' : 'bg-dark' }} rounded-circle d-flex justify-content-center align-items-center me-2"
                                    style="width: 2rem; height: 2rem;">
                                    @isset($delivery->order_packing_date) <i class="fa fa-check text-white p-0"></i> @endisset
                                </div>
                                <span class="text-dark">----------</span>
                                <h6 class="m-0 ms-2">
                                    The order packing was done at <span class="fw-bolder">{{ $delivery->order_packing_date ?? '-' }}</span>
                                </h6>
                            </div>

                            <div class="col-md-12 d-flex align-items-center my-1">
                                <div class="bg-transparent d-flex justify-content-center align-items-center"
                                    style="width: 2rem; height: 2rem;">
                                    <i class="fa fa-arrow-down text-dark"></i>
                                </div>
                            </div>
                            
                            <div class="col-md-12 d-flex align-items-center my-2">
                                <div class="{{ isset($delivery->start_delivering_date) ? 'bg-success' : 'bg-dark' }} rounded-circle d-flex justify-content-center align-items-center me-2"
                                    style="width: 2rem; height: 2rem;">
                                    @isset($delivery->start_delivering_date) <i class="fa fa-check text-white p-0"></i> @endisset
                                </div>
                                <span class="text-dark">----------</span>
                                <h6 class="m-0 ms-2">
                                    The delivery of the product started at <span class="fw-bolder">{{ $delivery->start_delivering_date ?? '-' }}</span>
                                </h6>
                            </div>

                            <div class="col-md-12 d-flex align-items-center my-1">
                                <div class="bg-transparent d-flex justify-content-center align-items-center"
                                    style="width: 2rem; height: 2rem;">
                                    <i class="fa fa-arrow-down text-dark"></i>
                                </div>
                            </div>
                            
                            <div class="col-md-12 d-flex align-items-center my-2">
                                <div class="{{ isset($delivery->product_received_date) ? 'bg-success' : 'bg-dark' }} rounded-circle d-flex justify-content-center align-items-center me-2"
                                    style="width: 2rem; height: 2rem;">
                                    @isset($delivery->product_received_date) <i class="fa fa-check text-white p-0"></i> @endisset
                                </div>
                                <span class="text-dark">----------</span>
                                <h6 class="m-0 ms-2">
                                    The customer received the product at <span class="fw-bolder">{{ $delivery->product_received_date ?? '-' }}</span>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row align-items-start justify-content-between mx-5 mb-4">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header pb-1">
                            <h5>Destination</h5>
                        </div>
                        <div class="card-body pt-0 pb-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Address</label>
                                        <input class="form-control ps-2 pt-0 border-0 bg-transparent"
                                            type="text" name="address" value="{{ $delivery->address }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">City</label>
                                        <input class="form-control ps-2 pt-0 border-0 bg-transparent"
                                            type="text" name="city" value="{{ $delivery->city }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Country</label>
                                        <input class="form-control ps-2 pt-0 border-0 bg-transparent"
                                            type="text" name="country" value="{{ $delivery->country }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Distance (Estimated)</label>
                                        <input class="form-control ps-2 pt-0 border-0 bg-transparent"
                                            type="text" name="distance" value="{{ $delivery->getDistanceInKm() }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3 px-1 align-items-center">
                        <div class="col-md-12 d-flex justify-content-start">
                            <a class="btn btn-success font-weight-bold shadow-sm position-relative me-2" style="z-index: 10;"
                                href="{{ route('delivery.edit', ['id' => $delivery->id]) }}">
                                <i class="fa fa-edit"></i>
                                <span>Edit</span>
                            </a>
                            <form role="form" method="post" action="{{ route('delivery.delete', ['id' => $delivery->id]) }}" id="delete-form">
                                @method('DELETE')
                                @csrf
                                <a class="btn btn-danger font-weight-bold shadow-sm position-relative" style="z-index: 10;"
                                    href="{{ route('delivery.delete', ['id' => $delivery->id]) }}"
                                    onclick="event.preventDefault(); document.getElementById('delete-form').submit();">
                                    <i class="fa fa-trash"></i>
                                    <span>Delete</span>
                                </a>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header pb-1">
                            <h5>Customer Contact</h5>
                        </div>
                        <div class="card-body pt-0 pb-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Name</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="name" value="{{ $delivery->sale->customer->name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Email address</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="email" name="email" value="{{ $delivery->sale->customer->email }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Phone number</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="phone" value="{{ $delivery->sale->customer->phone }}" disabled>
                                    </div>
                                </div>
                                <a class="btn btn-primary font-weight-bold shadow-sm"
                                    href="{{ route('customer.show', ['id' => $delivery->sale->customer_id]) }}">
                                    <span>View More Details</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @include('layouts.footers.auth.footer')
    </div>
@endsection

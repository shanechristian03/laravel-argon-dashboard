@php
    $update = $update ?? false;

    if ($update) {
        $name = $lead->name;
        $email = $lead->email;
        $phone = $lead->phone;
        $company_name = $lead->company_name;
        $company_address = $lead->company_address;
        $company_city = $lead->company_city;
        $company_country = $lead->company_country;
        $company_zip_code = $lead->company_zip_code;
        $company_website = $lead->company_website;
    }

    if (old('name') != null) {
        $name = old('name');
    }
    if (old('email') != null) {
        $email = old('email');
    }
    if (old('phone') != null) {
        $phone = old('phone');
    }
    if (old('company_name') != null) {
        $company_name = old('company_name');
    }
    if (old('company_address') != null) {
        $company_address = old('company_address');
    }
    if (old('company_city') != null) {
        $company_city = old('company_city');
    }
    if (old('company_country') != null) {
        $company_country = old('company_country');
    }
    if (old('company_zip_code') != null) {
        $company_zip_code = old('company_zip_code');
    }
    if (old('company_website') != null) {
        $company_website = old('company_website');
    }
@endphp

@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => $update ? 'Edit Lead' : 'Add New Lead'])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="container-fluid py-4">
        <div class="row justify-content-center">

            @if (!session()->has('error'))
                <div class="col-md-8">
                    <div class="card">
                        <form role="form" method="POST" enctype="multipart/form-data"
                            action={{ $update ? route('lead.update', ['id' => $lead->id]) : route('lead.store') }}>
                            @method($update ? 'PATCH' : 'POST')
                            @csrf
                            <div class="card-header pt-3 pb-0">
                                <h5>{{ $update ? "Edit Lead (ID: $lead->id)" : 'Add New Lead' }}</h5>
                            </div>

                            <div class="card-body pt-0">
                                <hr class="horizontal dark">
                                <p class="text-uppercase fw-bold">Contact Details</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Name</label>
                                            <input class="form-control ps-2 pt-1" type="text" name="name"
                                                value="{{ $name ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Email address</label>
                                            <input class="form-control ps-2 pt-1" type="email" name="email"
                                                value="{{ $email ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Phone number</label>
                                            <input class="form-control ps-2 pt-1" type="text" name="phone"
                                                value="{{ $phone ?? '' }}">
                                        </div>
                                    </div>
                                </div>
                                <hr class="horizontal dark">
                                <p class="text-uppercase fw-bold">Company Details</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Company Name</label>
                                            <input class="form-control ps-2 pt-1" type="text" name="company_name"
                                                value="{{ $company_name ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Address</label>
                                            <input class="form-control ps-2 pt-1" type="text" name="company_address"
                                                value="{{ $company_address ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">City</label>
                                            <input class="form-control ps-2 pt-1" type="text" name="company_city"
                                                value="{{ $company_city ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Country</label>
                                            <input class="form-control ps-2 pt-1" type="text" name="company_country"
                                                value="{{ $company_country ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Zip Code</label>
                                            <input class="form-control ps-2 pt-1" type="text" name="company_zip_code"
                                                value="{{ $company_zip_code ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Website URL</label>
                                            <input class="form-control ps-2 pt-1" type="text" name="company_website"
                                                value="{{ $company_website ?? '' }}">
                                        </div>
                                    </div>
                                    <button class="btn btn-primary text-center mt-3">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        </div>
        @include('layouts.footers.auth.footer')
    </div>
@endsection

@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => $lead->name . ' (Lead)'])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="container-fluid py-4">
        <div class="row">

            @if (!session()->has('error'))
                <div class="col-md-6 d-flex flex-column">
                    <div class="card">
                        <div class="card-body pb-0">
                            <p class="text-uppercase fw-bold">Contact Details</p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Name</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="name" value="{{ $lead->name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Email address</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="email" name="email" value="{{ $lead->email }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Phone number</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="phone" value="{{ $lead->phone }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3 ps-1 align-items-center">
                        <div class="col-md-6">
                            <form role="form" method="post" action="{{ route('lead.convert', ['id' => $lead->id]) }}" id="convert-form">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-primary shadow-sm position-relative opacity-9" style="z-index: 10;"
                                    onclick="event.preventDefault(); document.getElementById('convert-form').submit();">
                                    Convert To Customer
                                </button>
                            </form>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                            <a class="btn btn-success font-weight-bold shadow-sm position-relative me-1" style="z-index: 10;"
                                href="{{ route('lead.edit', ['id' => $lead->id]) }}">
                                <i class="fa fa-edit"></i>
                                <span>Edit</span>
                            </a>
                            <form role="form" method="post" action="{{ route('lead.delete', ['id' => $lead->id]) }}" id="delete-form">
                                @method('DELETE')
                                @csrf
                                <a class="btn btn-danger font-weight-bold shadow-sm position-relative" style="z-index: 10;"
                                    href="{{ route('lead.delete', ['id' => $lead->id]) }}"
                                    onclick="event.preventDefault(); document.getElementById('delete-form').submit();">
                                    <i class="fa fa-trash"></i>
                                    <span>Delete</span>
                                </a>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <p class="text-uppercase fw-bold">Company Details</p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Company Name</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="company_name" value="{{ $lead->company_name ?? '-' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Address</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="company_address" value="{{ $lead->company_address ?? '-' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">City</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="company_city" value="{{ $lead->company_city ?? '-' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Country</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="company_country" value="{{ $lead->company_country ?? '-' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Zip Code</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="company_zip_code" value="{{ $lead->company_zip_code ?? '-' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Website URL</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="company_website" value="{{ $lead->company_website ?? '-' }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            
        @include('layouts.footers.auth.footer')
    </div>
@endsection

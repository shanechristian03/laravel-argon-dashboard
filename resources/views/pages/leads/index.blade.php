@extends('layouts.app')

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'Lead Management'])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="row mt-4 mb-6 mx-4">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between pb-0">
                    <h4>Leads</h4>
                    <a class="btn btn-primary font-weight-bold py-2 px-3" href="{{ route('lead.create') }}">
                        <i class="fa fa-plus"></i>
                        <span>Add Lead</span>
                    </a>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0 pt-2 table-paginate">
                        <table class="table align-items-center mb-0 mt-2">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Email</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Phone</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Company</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($leads as $lead)
                                    <tr onclick="window.location = '{{ route('lead.show', ['id' => $lead->id]) }}';" style="cursor: pointer;">
                                        <td class="ps-4">
                                            <p class="text-sm font-weight-bold mb-0">{{ $lead->name }}</p>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{ $lead->email }}</p>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{ $lead->phone }}</p>
                                        </td>
                                        <td>
                                            <p class="text-sm text-center font-weight-bold mb-0">{{ $lead->company_name ?? '-' }}</p>
                                        </td>
                                        <td class="align-middle text-center py-1">
                                            <div class="d-flex py-1 justify-content-center align-items-center">
                                                <a class="btn btn-success font-weight-bold me-1 mb-0 p-2"
                                                    style="font-size: 0.75rem;"
                                                    href="{{ route('lead.edit', ['id' => $lead->id]) }}"
                                                    onclick="event.stopPropagation();">
                                                    <i class="fa fa-edit"></i>
                                                    <span>Edit</span>
                                                </a>
                                                <form role="form" method="post" action="{{ route('lead.delete', ['id' => $lead->id]) }}" id="delete-form-{{ $lead->id }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a class="btn btn-danger font-weight-bold mb-0 p-2"
                                                        style="font-size: 0.75rem;"
                                                        href="{{ route('lead.delete', ['id' => $lead->id]) }}"
                                                        onclick="event.preventDefault(); event.stopPropagation(); document.getElementById('delete-form-{{ $lead->id }}').submit();">
                                                        <i class="fa fa-trash"></i>
                                                        <span>Delete</span>
                                                    </a>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                {{ $leads->links() }}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

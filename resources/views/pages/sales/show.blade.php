@php
    $total = 0;
    $final_discount = 0;
    $tax = 0;
@endphp

@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => "Sale ID $sale->id"])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="container-fluid py-4">
        @if (!session()->has('error'))
            <div class="row">
                <div class="card mb-4">
                    <div class="card-header pb-0 pt-4">
                        <div class="row m-0 p-0 align-items-center justify-content-between">
                            <h5 class="col-md-8 p-0">Product(s) in Sale ID {{ $sale->id }}</h5>
                            <div class="form-group col-md-2 p-0 mb-2 d-flex align-items-center">
                                <h6 for="example-text-input" class="form-control-label m-0">Date: </h6>
                                <input class="form-control py-0 ps-2 pe-0 border-0 bg-transparent fs-6 fw-semibold"
                                    type="text" name="sale_date" value="{{ $sale->sale_date }}" disabled>
                            </div>
                        </div>               
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Price</th>
                                        <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7 px-2">Qty</th>
                                        <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7 px-2">Disc (%)</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Discount</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sale->details as $sale_detail)
                                        @php
                                            $total += $sale_detail->subtotal;
                                        @endphp
                                        <tr>
                                            <td class="ps-4">
                                                <p class="text-sm font-weight-bold mb-0">{{ $sale_detail->product->name }}</p>
                                            </td>
                                            <td>
                                                <p class="text-sm font-weight-bold mb-0" style="color:rgba(41, 101, 101, 0.8);">
                                                    {{ $sale_detail->getFormattedPrice($sale_detail->price) }}
                                                </p>
                                            </td>
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0">{{ $sale_detail->qty }}</p>
                                            </td>
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0">{{ $sale_detail->discount }}</p>
                                            </td>
                                            <td>
                                                <p class="text-sm font-weight-bold mb-0" style="color:rgba(41, 101, 101, 0.8);">
                                                    {{ $sale_detail->getFormattedPrice(floor($sale_detail->discount * $sale_detail->price * $sale_detail->qty / 100)) }}
                                                </p>
                                            </td>
                                            <td>
                                                <p class="text-sm font-weight-bold mb-0" style="color:rgba(41, 101, 101, 0.8);">
                                                    {{ $sale_detail->getFormattedPrice($sale_detail->subtotal) }}
                                                </p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-start justify-content-between mb-4">
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header pb-1">
                            <h5>Customer Contact</h5>
                        </div>
                        <div class="card-body pt-0 pb-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Name</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="name" value="{{ $sale->customer->name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Email address</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="email" name="email" value="{{ $sale->customer->email }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Phone number</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="phone" value="{{ $sale->customer->phone }}" disabled>
                                    </div>
                                </div>
                                <a class="btn btn-primary font-weight-bold shadow-sm"
                                    href="{{ route('customer.show', ['id' => $sale->customer->id]) }}">
                                    <span>View More Details</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body pt-3 pb-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row align-items-center justify-content-between pe-4">
                                        <h6 class="col-md-4 fw-bold">Total</h6>
                                        <div class="col-md-8 text-end fw-bold" style="color:rgba(41, 101, 101, 0.8);">
                                            {{ $sale->getFormattedPrice($total) }}
                                        </div>
                                    </div>
                                </div>
                                <hr class="horizontal dark">
                                <div class="col-md-12">
                                    <div class="row align-items-center justify-content-between pe-4">
                                        <h6 class="col-md-4 fw-bold">Discount ({{ $sale->discount }}%)</h6>
                                        <div class="col-md-8 text-end fw-bold text-danger">
                                            @php
                                                $final_discount = floor($sale->discount * $total / 100);
                                            @endphp
                                            - {{ $sale->getFormattedPrice($final_discount) }}
                                        </div>
                                    </div>
                                </div>
                                <hr class="horizontal dark">
                                <div class="col-md-12">
                                    <div class="row align-items-center justify-content-between pe-4">
                                        <h6 class="col-md-4 fw-bold">PPN (11%)</h6>
                                        <div class="col-md-8 text-end fw-bold text-success">
                                            @php
                                                $tax = floor(11 * ($total - $final_discount) / 100);
                                            @endphp
                                            + {{ $sale->getFormattedPrice($tax) }}
                                        </div>
                                    </div>
                                </div>
                                <hr class="horizontal dark">
                                <div class="col-md-12">
                                    <div class="row align-items-center justify-content-between pe-4">
                                        <h4 class="col-md-4 fw-bold">Grand Total</h4>
                                        <h4 class="col-md-8 text-end fw-bolder" style="color:rgba(41, 101, 101, 0.8);">
                                            {{ $sale->getFormattedPrice($total - $final_discount + $tax) }}
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3 px-1 align-items-center">
                        <div class="col-md-12 d-flex justify-content-end">
                            <a class="btn btn-success font-weight-bold shadow-sm position-relative me-2" style="z-index: 10;"
                                href="{{ route('sale.edit', ['id' => $sale->id]) }}">
                                <i class="fa fa-edit"></i>
                                <span>Edit</span>
                            </a>
                            <form role="form" method="post" action="{{ route('sale.delete', ['id' => $sale->id]) }}" id="delete-form">
                                @method('DELETE')
                                @csrf
                                <a class="btn btn-danger font-weight-bold shadow-sm position-relative" style="z-index: 10;"
                                    href="{{ route('sale.delete', ['id' => $sale->id]) }}"
                                    onclick="event.preventDefault(); document.getElementById('delete-form').submit();">
                                    <i class="fa fa-trash"></i>
                                    <span>Delete</span>
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @include('layouts.footers.auth.footer')
    </div>
@endsection

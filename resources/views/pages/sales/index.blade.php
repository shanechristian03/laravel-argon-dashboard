@extends('layouts.app')

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'Sale Management'])
    <div id="alert">
        @include('components.alert')
    </div>

    @if (!session()->has('error'))
        <div class="row mt-4 mb-6 mx-4">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between pb-0">
                        <h4>Sales</h4>
                        <a class="btn btn-primary font-weight-bold py-2 px-3" href="{{ route('sale.create') }}">
                            <i class="fa fa-plus"></i>
                            <span>Add Sale</span>
                        </a>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0 pt-2 table-paginate">
                            <table class="table align-items-center mb-0 mt-2">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">ID</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Customer</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Date</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Total</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sales as $sale)
                                        <tr onclick="window.location = '{{ route('sale.show', ['id' => $sale->id]) }}';" style="cursor: pointer;">
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0">{{ $sale->id }}</p>
                                            </td>
                                            <td>
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $sale->customer->name }}</h6>
                                                    @isset($sale->customer->company_name)
                                                        <p class="mb-0 text-sm">from&nbsp;{{ $sale->customer->company_name }}</p>
                                                    @endisset
                                                </div>
                                            </td>
                                            <td>
                                                <p class="text-sm font-weight-bold mb-0">{{ $sale->sale_date }}</p>
                                            </td>
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0" style="color:rgba(41, 101, 101, 0.8);">
                                                    {{ $sale->getFormattedPrice($sale->total) }}
                                                </p>
                                            </td>
                                            <td class="align-middle text-center py-1">
                                            <div class="d-flex py-1 justify-content-center align-items-center">
                                                <a class="btn btn-success font-weight-bold me-1 mb-0 p-2"
                                                    style="font-size: 0.75rem;"
                                                    href="{{ route('sale.edit', ['id' => $sale->id]) }}"
                                                    onclick="event.stopPropagation();">
                                                    <i class="fa fa-edit"></i>
                                                    <span>Edit</span>
                                                </a>
                                                <form role="form" method="post" action="{{ route('sale.delete', ['id' => $sale->id]) }}" id="delete-form-{{ $sale->id }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a class="btn btn-danger font-weight-bold mb-0 p-2"
                                                        style="font-size: 0.75rem;"
                                                        href="{{ route('sale.delete', ['id' => $sale->id]) }}"
                                                        onclick="event.preventDefault(); event.stopPropagation(); document.getElementById('delete-form-{{ $sale->id }}').submit();">
                                                        <i class="fa fa-trash"></i>
                                                        <span>Delete</span>
                                                    </a>
                                                </form>
                                            </div>
                                        </td>
                                        </tr>
                                    @endforeach
                                    {{ $sales->links() }}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

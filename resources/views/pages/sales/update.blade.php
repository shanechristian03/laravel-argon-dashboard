@php
    $counter = 1;
    $total = 0;
    $final_discount = 0;
    $tax = 0;
@endphp

@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => "Edit Sale ID $sale->id"])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="container-fluid py-4">
        @if (!session()->has('error'))
            <div class="row mb-4">
                <div class="card">
                    <div class="card-header pt-3 pb-1">
                        <div class="row m-0 p-0 align-items-center justify-content-between">
                            <h6 class="col-md-9 p-0">Edit Sale ID {{ $sale->id }}</h6>
                            <div class="col-md-3 p-0">
                                <input type="date" class="form-control" name="sale_date" value="{{ $sale->sale_date }}" />
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0 pb-2">
                        <div class="row justify-content-between">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Product Name</label>
                                    <select class="form-control ps-2 pt-1" type="text" name="product_name" onchange="onProductChange(this)">
                                        @foreach ($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Price</label>
                                    <input class="form-control ps-2 pt-1" type="number" name="price" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Quantity</label>
                                    <input class="form-control ps-2 pt-1" type="number" name="qty" onchange="onQtyChange(this)">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Discount (%)</label>
                                    <input class="form-control ps-2 pt-1" type="number" name="product_discount" onchange="onProductDiscountChange(this)">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Discount Value</label>
                                    <input class="form-control ps-2 pt-1" type="number" name="product_discount_value" disabled>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Subtotal</label>
                                    <input class="form-control ps-2 pt-1" type="number" name="subtotal" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-end pe-3">
                            <button class="btn btn-primary py-2" style="width: 10%;" onclick="addProduct()">
                                <i class="fa fa-plus"></i>
                                <span class="ms-1">Add</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h5>Product(s) in Sale</h5>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Price</th>
                                        <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7 px-2">Qty</th>
                                        <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7 px-2">Disc (%)</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Subtotal</th>
                                        <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sale->details as $sale_detail)
                                        @php
                                            $total += $sale_detail->subtotal;
                                        @endphp
                                        <tr id="row-{{ $counter }}">
                                            <td class="ps-4">
                                                <input name="product_id" disabled hidden value="{{ $sale_detail->product->id }}">
                                                <p class="text-sm font-weight-bold mb-0">{{ $sale_detail->product->name }}</p>
                                            </td>
                                            <td>
                                                <p class="text-sm font-weight-bold mb-0 price" style="color:rgba(41, 101, 101, 0.8);">
                                                    {{ $sale_detail->getFormattedPrice($sale_detail->price) }}
                                                </p>
                                            </td>
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0 qty">{{ $sale_detail->qty }}</p>
                                            </td>
                                            <td>
                                                <p class="text-sm text-center font-weight-bold mb-0 product-discount">{{ $sale_detail->discount }}</p>
                                            </td>
                                            <td>
                                                <p class="text-sm font-weight-bold mb-0 subtotal" style="color:rgba(41, 101, 101, 0.8);">
                                                    {{ $sale_detail->getFormattedPrice($sale_detail->subtotal) }}
                                                </p>
                                            </td>
                                            <td class="align-middle text-center py-1">
                                                <button class="btn btn-danger font-weight-bold mb-0 p-2" id="btn-row-{{ $counter }}" onclick="removeProduct(this)">
                                                    <i class="fa fa-trash"></i>
                                                    <span>Remove</span>
                                                </button>
                                            </td>
                                        </tr>
                                        @php
                                            $counter += 1
                                        @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row align-items-start justify-content-between mb-4">
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header pb-1">
                            <h5>Customer Contact</h5>
                        </div>
                        <div class="card-body pt-0 pb-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Name</label>
                                        <select class="form-control ps-2 pt-1" type="text" name="customer_name" onchange="onCustomerChange(this)">
                                            @foreach ($customers as $customer)
                                                <option value="{{ $customer->id }}" @selected($sale->customer_id == $customer->id)>
                                                    {{ $customer->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Email address</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="email" name="email" value="{{ $sale->customer->email }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Phone number</label>
                                        <input class="form-control ps-2 pt-1 border-0 bg-transparent"
                                            type="text" name="phone" value="{{ $sale->customer->phone }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row justify-content-end">
                        <div class="form-group w-25">
                            <label for="example-text-input" class="form-control-label">Discount (%)</label>
                            <input class="form-control ps-2 pt-1" type="number" name="discount" value="{{ $sale->discount }}" onchange="onDiscountChange(this)">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body pt-3 pb-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row align-items-center justify-content-between pe-4">
                                        <h6 class="col-md-4 fw-bold">Total</h6>
                                        <div class="col-md-8 text-end fw-bold" id="total" style="color:rgba(41, 101, 101, 0.8);">
                                            {{ $sale->getFormattedPrice($total) }}
                                        </div>
                                    </div>
                                </div>
                                <hr class="horizontal dark">
                                <div class="col-md-12">
                                    <div class="row align-items-center pe-4">
                                        <h6 class="col-md-4 fw-bold" id="final-discount-label">Discount ({{ $sale->discount }}%)</h6>
                                        <div class="col-md-8 text-end fw-bold text-danger" id="final-discount">
                                            @php
                                                $final_discount = floor($sale->discount * $total / 100);
                                            @endphp
                                            - {{ $sale->getFormattedPrice($final_discount) }}
                                        </div>
                                    </div>
                                </div>
                                <hr class="horizontal dark">
                                <div class="col-md-12">
                                    <div class="row align-items-center justify-content-between pe-4">
                                        <h6 class="col-md-4 fw-bold">PPN (11%)</h6>
                                        <div class="col-md-8 text-end fw-bold text-success" id="tax">
                                            @php
                                                $tax = floor(11 * ($total - $final_discount) / 100);
                                            @endphp
                                            + {{ $sale->getFormattedPrice($tax) }}
                                        </div>
                                    </div>
                                </div>
                                <hr class="horizontal dark">
                                <div class="col-md-12">
                                    <div class="row align-items-center justify-content-between pe-4">
                                        <h4 class="col-md-4 fw-bold">Grand Total</h4>
                                        <h4 class="col-md-8 text-end fw-bolder" id="grand-total" style="color:rgba(41, 101, 101, 0.8);">
                                            {{ $sale->getFormattedPrice($total - $final_discount + $tax) }}
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3 px-3">
                        {{ csrf_field() }}
                        <button class="btn btn-success py-2" onclick="saveSale()">
                            <i class="fa fa-save"></i>
                            <span class="ms-1">Save Sale Data</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        @include('layouts.footers.auth.footer')
    </div>

    <script src="/assets/js/sales.js"></script>
    <script>
        counter = {{ $counter }}

        const saveSale = async function () {
            const _token = document.querySelector('input[name="_token"]').value
            const customer_id = document.querySelector('select[name="customer_name"]').value
            const sale_date = document.querySelector('input[name="sale_date"]').value
            const discount = document.querySelector('input[name="discount"]').value
            const tax = reverseNumberFormat(document.querySelector('#tax').innerText.slice(2))
            const total = reverseNumberFormat(document.querySelector('#grand-total').innerText)
            const details = []

            const rows = document.querySelectorAll('tbody tr')
            for (let i = 0; i < rows.length; i++) {
                details.push({
                    product_id: document.querySelector(`#${rows[i].id} input[name="product_id"]`).value,
                    price: reverseNumberFormat(document.querySelector(`#${rows[i].id} .price`).innerText),
                    qty: document.querySelector(`#${rows[i].id} .qty`).innerText,
                    discount: document.querySelector(`#${rows[i].id} .product-discount`).innerText,
                    subtotal: reverseNumberFormat(document.querySelector(`#${rows[i].id} .subtotal`).innerText),
                })
            }

            const saleData = {
                _token,
                customer_id,
                sale_date,
                discount,
                tax,
                total,
                details
            }
            
            await fetch('http://localhost:8000/sale/{{ $sale->id }}/update', {
                method: 'PATCH',
                redirect: 'follow',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(saleData)
            })

            window.location = 'http://localhost:8000/sale/{{ $sale->id }}'
        }

        window.onload = function () {
            document.querySelector('select[name="product_name"]').onchange()
            document.querySelector('input[name="qty"]').onchange()
            document.querySelector('input[name="product_discount"]').onchange()
        }
    </script>
@endsection
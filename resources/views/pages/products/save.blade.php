@php
    $update = $update ?? false;

    if ($update) {
        $name = $product->name;
        $price = $product->price;
        $stock = $product->stock;
    }

    if (old('name') != null) {
        $name = old('name');
    }
    if (old('price') != null) {
        $price = old('price');
    }
    if (old('stock') != null) {
        $stock = old('stock');
    }
@endphp

@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => $update ? 'Edit Product' : 'Add New Product'])
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="container-fluid py-3">
        <div class="row justify-content-center mb-4">

            @if (!session()->has('error'))
                <div class="col-md-6">
                    <div class="card">
                        <form role="form" method="POST" enctype="multipart/form-data"
                            action={{ $update ? route('product.update', ['id' => $product->id]) : route('product.store') }}>
                            @method($update ? 'PATCH' : 'POST')
                            @csrf
                            <div class="card-header pt-3 pb-0">
                                <h5>{{ $update ? "Edit Product (ID: $product->id)" : 'Add New Product' }}</h5>
                            </div>

                            <div class="card-body pt-0">
                                <hr class="horizontal dark">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Product Name</label>
                                            <input class="form-control ps-2 pt-1" type="text" name="name"
                                                value="{{ $name ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Price</label>
                                            <input class="form-control ps-2 pt-1" type="number" name="price"
                                                value="{{ $price ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Stock</label>
                                            <input class="form-control ps-2 pt-1" type="number" name="stock"
                                                value="{{ $stock ?? '' }}">
                                        </div>
                                    </div>
                                    <button class="btn btn-primary text-center mt-3">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        </div>
        @include('layouts.footers.auth.footer')
    </div>
@endsection

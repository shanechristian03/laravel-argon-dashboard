// Formatter
var counter = 1

const IDRupiah = new Intl.NumberFormat('id-ID', {
    style: 'currency',
    currency: 'IDR',
})

const reverseNumberFormat = function (stringNumber) {
    return parseFloat(stringNumber
        .replace(/\./g, '')
        .replace(/,/g, '.')
        .slice(3)
    )
}


// Calculation
const calculateDiscountValue = function () {
    const price = document.querySelector('input[name="price"]').value
    const qty = document.querySelector('input[name="qty"]').value
    const discount = document.querySelector('input[name="product_discount"]').value

    const discount_value = document.querySelector('input[name="product_discount_value"]')
    discount_value.value = Math.floor(discount * price * qty / 100)

    calculateSubTotal()
}

const calculateSubTotal = function () {
    const price = document.querySelector('input[name="price"]').value
    const qty = document.querySelector('input[name="qty"]').value
    const discount_value = document.querySelector('input[name="product_discount_value"]').value

    const subtotal = document.querySelector('input[name="subtotal"]')
    subtotal.value = price * qty - discount_value
}

const calculateTotal = function () {
    const subtotal = document.querySelectorAll('p.subtotal')
    let total = 0

    for (let i = 0; i < subtotal.length; i++) {
        total += reverseNumberFormat(subtotal[i].innerText)
    }
    document.querySelector('#total').innerText = IDRupiah.format(total)

    calculateFinalDiscountValue()
}

const calculateFinalDiscountValue = function () {
    const total = reverseNumberFormat(document.querySelector('#total').innerText)
    const discount = document.querySelector('input[name="discount"]').value
    const finalDiscount = Math.floor(discount * total / 100)

    document.querySelector('#final-discount-label').innerText = `Discount (${discount}%)`
    document.querySelector('#final-discount').innerText = '- ' + IDRupiah.format(finalDiscount)

    calculateTax()
}

const calculateTax = function () {
    const total = reverseNumberFormat(document.querySelector('#total').innerText)
    const finalDiscount = reverseNumberFormat(document.querySelector('#final-discount').innerText.slice(2))
    const tax = Math.floor(11 * (total - finalDiscount) / 100)

    document.querySelector('#tax').innerText = '+ ' + IDRupiah.format(tax)

    calculateGrandTotal()
}

const calculateGrandTotal = function () {
    const total = reverseNumberFormat(document.querySelector('#total').innerText)
    const finalDiscount = reverseNumberFormat(document.querySelector('#final-discount').innerText.slice(2))
    const tax = reverseNumberFormat(document.querySelector('#tax').innerText.slice(2))

    document.querySelector('#grand-total').innerText = IDRupiah.format(total - finalDiscount + tax)
}


// Event handler
const onProductChange = async function (el) {
    const response = await fetch('http://localhost:8000/api/product/' + el.value)
    const product = await response.json()

    const price = document.querySelector('input[name="price"]')
    price.value = product.price

    calculateDiscountValue()
}

const onQtyChange = function (el) {
    if (!el.value || el.value < 0) {
        el.value = 0
    }
    calculateDiscountValue()
}

const onProductDiscountChange = function (el) {
    if (!el.value || el.value < 0) {
        el.value = 0
    }
    calculateDiscountValue()
}

const addProduct = function () {
    const product_id = document.querySelector('select[name="product_name"]').value
    const product_name = document.querySelector(`option[value="${product_id}"]`).innerText
    const price = document.querySelector('input[name="price"]').value
    const qty = document.querySelector('input[name="qty"]').value
    const discount = document.querySelector('input[name="product_discount"]').value
    const subtotal = document.querySelector('input[name="subtotal"]').value

    const newDocument = new DOMParser().parseFromString(`<table>
        <tr id="row-${counter}">
            <td class="ps-4">
                <input name="product_id" disabled hidden value="${product_id}">
                <p class="text-sm font-weight-bold mb-0">${product_name}</p>
            </td>
            <td>
                <p class="text-sm font-weight-bold mb-0 price" style="color:rgba(41, 101, 101, 0.8);">
                    ${IDRupiah.format(price)}
                </p>
            </td>
            <td>
                <p class="text-sm text-center font-weight-bold mb-0 qty">${qty}</p>
            </td>
            <td>
                <p class="text-sm text-center font-weight-bold mb-0 product-discount">${discount}</p>
            </td>
            <td>
                <p class="text-sm font-weight-bold mb-0 subtotal" style="color:rgba(41, 101, 101, 0.8);">
                    ${IDRupiah.format(subtotal)}
                </p>
            </td>
            <td class="align-middle text-center py-1">
                <button class="btn btn-danger font-weight-bold mb-0 p-2" id="btn-row-${counter}" onclick="removeProduct(this)">
                    <i class="fa fa-trash"></i>
                    <span>Remove</span>
                </button>
            </td>
        </tr>
    </table>`, 'text/html')

    document.querySelector('tbody').append(newDocument.querySelector('tr'))
    counter += 1
    clearProduct()
    calculateTotal()       
}

const removeProduct = function (el) {
    const rowId = el.id.slice(4)
    document.querySelector(`#${rowId}`).remove()
    calculateTotal() 
}

const clearProduct = function () {
    document.querySelector('input[name="qty"]').value = 0
    document.querySelector('input[name="product_discount"]').value = 0
    document.querySelector('input[name="product_discount_value"]').value = 0
    document.querySelector('input[name="subtotal"]').value = 0
}

const onCustomerChange = async function (el) {
    const response = await fetch('http://localhost:8000/api/customer/' + el.value)
    const customer = await response.json()

    const email = document.querySelector('input[name="email"]')
    const phone = document.querySelector('input[name="phone"]')

    email.value = customer.email
    phone.value = customer.phone
}

const onDiscountChange = function (el) {
    if (!el.value || el.value < 0) {
        el.value = 0
    }
    calculateFinalDiscountValue()
}
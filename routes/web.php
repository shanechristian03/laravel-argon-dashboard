<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ResetPassword;
use App\Http\Controllers\ChangePassword;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SalesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::group(['middleware' => 'guest'], function () {
	Route::get('/register', [RegisterController::class, 'create'])->name('register');
	Route::post('/register', [RegisterController::class, 'store'])->name('register.perform');
	Route::get('/login', [LoginController::class, 'show'])->name('login');
	Route::post('/login', [LoginController::class, 'login'])->name('login.perform');
	Route::get('/login/google', [LoginController::class, 'google'])->name('login.google');
	Route::get('/login/google/redirect', [LoginController::class, 'redirectFromGoogle'])->name('login.google.perform');
	Route::get('/reset-password', [ResetPassword::class, 'show'])->name('reset-password');
	Route::post('/reset-password', [ResetPassword::class, 'send'])->name('reset.perform');
	Route::get('/change-password', [ChangePassword::class, 'show'])->name('change-password');
	Route::post('/change-password', [ChangePassword::class, 'update'])->name('change.perform');
});


Route::group(['middleware' => 'auth'], function () {
	Route::get('/', function () { return redirect('/dashboard'); });
	Route::get('/dashboard', function () {
		return redirect('/sales');
	})->name('home');

	Route::get('/profile', [UserController::class, 'showProfile'])->name('profile');
	Route::patch('/profile', [UserController::class, 'updateProfile'])->name('profile.update');
	Route::get('/profile/{id}', [UserController::class, 'viewUser'])->name('profile.view');
	Route::get('/user-management', [UserController::class, 'viewAllUsers'])->name('user-management');
	Route::patch('/user/{id}/activate', [UserController::class, 'activate'])->name('user.activate');
	Route::patch('/user/{id}/deactivate', [UserController::class, 'deactivate'])->name('user.deactivate');

	Route::get('/leads', [LeadController::class, 'index'])->name('leads');
	Route::get('/lead/create', [LeadController::class, 'create'])->name('lead.create');
	Route::post('/lead/create', [LeadController::class, 'store'])->name('lead.store');
	Route::get('/lead/{id}', [LeadController::class, 'show'])->name('lead.show');
	Route::get('/lead/{id}/edit', [LeadController::class, 'edit'])->name('lead.edit');
	Route::patch('/lead/{id}/update', [LeadController::class, 'update'])->name('lead.update');
	Route::delete('/lead/{id}/delete', [LeadController::class, 'destroy'])->name('lead.delete');
	Route::delete('/lead/{id}/convert', [LeadController::class, 'convert'])->name('lead.convert');

	Route::get('/customers', [CustomerController::class, 'index'])->name('customers');
	Route::get('/customer/create', [CustomerController::class, 'create'])->name('customer.create');
	Route::post('/customer/create', [CustomerController::class, 'store'])->name('customer.store');
	Route::get('/customer/{id}', [CustomerController::class, 'show'])->name('customer.show');
	Route::get('/customer/{id}/edit', [CustomerController::class, 'edit'])->name('customer.edit');
	Route::patch('/customer/{id}/update', [CustomerController::class, 'update'])->name('customer.update');
	Route::patch('/customer/{id}/delete', [CustomerController::class, 'deactivate'])->name('customer.delete');

	Route::get('/products', [ProductController::class, 'index'])->name('products');
	Route::get('/product/create', [ProductController::class, 'create'])->name('product.create');
	Route::post('/product/create', [ProductController::class, 'store'])->name('product.store');
	Route::get('/product/{id}/edit', [ProductController::class, 'edit'])->name('product.edit');
	Route::patch('/product/{id}/update', [ProductController::class, 'update'])->name('product.update');
	Route::patch('/product/{id}/delete', [ProductController::class, 'deactivate'])->name('product.delete');

	Route::get('/sales', [SalesController::class, 'index'])->name('sales');
	Route::get('/sale/create', [SalesController::class, 'create'])->name('sale.create');
	Route::post('/sale/create', [SalesController::class, 'store'])->name('sale.store');
	Route::get('/sale/{id}', [SalesController::class, 'show'])->name('sale.show');
	Route::get('/sale/{id}/edit', [SalesController::class, 'edit'])->name('sale.edit');
	Route::patch('/sale/{id}/update', [SalesController::class, 'update'])->name('sale.update');
	Route::delete('/sale/{id}/delete', [SalesController::class, 'destroy'])->name('sale.delete');

	Route::get('/deliveries', [DeliveryController::class, 'index'])->name('deliveries');
	Route::get('/delivery/create', [DeliveryController::class, 'create'])->name('delivery.create');
	Route::post('/delivery/create', [DeliveryController::class, 'store'])->name('delivery.store');
	Route::get('/delivery/{id}', [DeliveryController::class, 'show'])->name('delivery.show');
	Route::get('/delivery/{id}/edit', [DeliveryController::class, 'edit'])->name('delivery.edit');
	Route::patch('/delivery/{id}/update', [DeliveryController::class, 'update'])->name('delivery.update');
	Route::delete('/delivery/{id}/delete', [DeliveryController::class, 'destroy'])->name('delivery.delete');

	Route::group(['prefix' => 'api'], function () {
		Route::get('/product/{id}', [ProductController::class, 'getProductDetails']);
		Route::get('/customer/{id}', [CustomerController::class, 'getCustomerDetails']);
	});
	
	Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
});
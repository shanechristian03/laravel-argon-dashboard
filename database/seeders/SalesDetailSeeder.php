<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalesDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('sales_detail')->truncate();
        DB::table('sales_detail')->insert([
            [
                'master_id' => '1',
                'product_id' => '1',
                'price' => 449000,
                'qty' => 3,
                'discount' => 20,
                'subtotal' => 1077600,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '2',
                'product_id' => '3',
                'price' => 649000,
                'qty' => 1,
                'discount' => 0,
                'subtotal' => 649000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '2',
                'product_id' => '5',
                'price' => 423000,
                'qty' => 1,
                'discount' => 0,
                'subtotal' => 423000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '3',
                'product_id' => '5',
                'price' => 423000,
                'qty' => 1,
                'discount' => 0,
                'subtotal' => 423000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '3',
                'product_id' => '6',
                'price' => 1649000,
                'qty' => 1,
                'discount' => 0,
                'subtotal' => 1649000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '4',
                'product_id' => '2',
                'price' => 2799000,
                'qty' => 1,
                'discount' => 10,
                'subtotal' => 2519100,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '5',
                'product_id' => '8',
                'price' => 1449000,
                'qty' => 1,
                'discount' => 25,
                'subtotal' => 1086750,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '5',
                'product_id' => '7',
                'price' => 269000,
                'qty' => 1,
                'discount' => 0,
                'subtotal' => 269000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '6',
                'product_id' => '2',
                'price' => 2799000,
                'qty' => 2,
                'discount' => 30,
                'subtotal' => 3918600,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '7',
                'product_id' => '11',
                'price' => 463000,
                'qty' => 5,
                'discount' => 0,
                'subtotal' => 2315000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '8',
                'product_id' => '11',
                'price' => 463000,
                'qty' => 1,
                'discount' => 0,
                'subtotal' => 463000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '8',
                'product_id' => '7',
                'price' => 269000,
                'qty' => 3,
                'discount' => 0,
                'subtotal' => 807000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '9',
                'product_id' => '1',
                'price' => 449000,
                'qty' => 2,
                'discount' => 0,
                'subtotal' => 898000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '10',
                'product_id' => '12',
                'price' => 649000,
                'qty' => 1,
                'discount' => 10,
                'subtotal' => 584100,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '11',
                'product_id' => '5',
                'price' => 423000,
                'qty' => 3,
                'discount' => 15,
                'subtotal' => 1078650,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'master_id' => '12',
                'product_id' => '12',
                'price' => 649000,
                'qty' => 1,
                'discount' => 10,
                'subtotal' => 584100,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}

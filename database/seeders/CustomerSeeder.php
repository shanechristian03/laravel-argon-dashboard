<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();
        DB::table('customers')->truncate();
        DB::table('customers')->insert([
            [
                'name' => 'Fikri',
                'email' => 'fikri@gmail.com',
                'phone' => '0826831745532',
                'company_name' => 'Grape',
                'company_address' => 'Fruit Street',
                'company_city' => 'Jakarta',
                'company_country' => 'Indonesia',
                'company_zip_code' => '74344',
                'company_website' => 'https://grape.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Yan',
                'email' => 'yannn24@gmail.com',
                'phone' => '0155283221',
                'company_name' => 'The Big Company',
                'company_address' => 'Bridgy Street',
                'company_city' => 'Kuala Lumpur',
                'company_country' => 'Malaysia',
                'company_zip_code' => '43664',
                'company_website' => 'https://the-big.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Vivi',
                'email' => 'vivi11@gmail.com',
                'phone' => '0125345678',
                'company_name' => 'Test Company',
                'company_address' => 'Great Sage Street',
                'company_city' => 'Singapore',
                'company_country' => 'Singapore',
                'company_zip_code' => '53566',
                'company_website' => 'https://test.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Tulen',
                'email' => 'tulen@gmail.com',
                'phone' => '081553820321',
                'company_name' => 'Another Company',
                'company_address' => 'Flower Street',
                'company_city' => 'Jakarta',
                'company_country' => 'Indonesia',
                'company_zip_code' => '72235',
                'company_website' => 'https://another.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Julia',
                'email' => 'julia832@gmail.com',
                'phone' => '040331632',
                'company_name' => 'Techhy',
                'company_address' => 'Hot Street',
                'company_city' => 'Canberra',
                'company_country' => 'Australia',
                'company_zip_code' => '21331',
                'company_website' => 'https://www.techhy.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Klein',
                'email' => 'klein0@gmail.com',
                'phone' => '084226888531',
                'company_name' => null,
                'company_address' => null,
                'company_city' => null,
                'company_country' => null,
                'company_zip_code' => null,
                'company_website' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Jefri',
                'email' => 'jefri00@gmail.com',
                'phone' => '081889027439',
                'company_name' => null,
                'company_address' => null,
                'company_city' => null,
                'company_country' => null,
                'company_zip_code' => null,
                'company_website' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Melisa',
                'email' => 'melisa@gmail.com',
                'phone' => '084990214244',
                'company_name' => 'Tic Tac',
                'company_address' => 'Testing Street',
                'company_city' => 'Medan',
                'company_country' => 'Indonesia',
                'company_zip_code' => '18053',
                'company_website' => 'https://tic-tac.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Yuji',
                'email' => 'yuji123@gmail.com',
                'phone' => '081234789655',
                'company_name' => 'Jonn',
                'company_address' => 'View Street',
                'company_city' => 'Bogor',
                'company_country' => 'Indonesia',
                'company_zip_code' => '67768',
                'company_website' => 'https://www.jonn.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Rani',
                'email' => 'rani63@gmail.com',
                'phone' => '081098748576',
                'company_name' => 'Lalalala',
                'company_address' => 'Milky Way Street',
                'company_city' => 'Depok',
                'company_country' => 'Indonesia',
                'company_zip_code' => '86112',
                'company_website' => 'https://lalalala.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
        Schema::enableForeignKeyConstraints();
    }
}

<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SalesMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();
        DB::table('sales_master')->truncate();
        DB::table('sales_master')->insert([
            [
                'customer_id' => '1',
                'sale_date' => '2023-08-31',
                'discount' => 0,
                'tax' => 118536,
                'total' => 1196136,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '2',
                'sale_date' => '2023-09-01',
                'discount' => 0,
                'tax' => 117920,
                'total' => 1189920,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '3',
                'sale_date' => '2023-09-02',
                'discount' => 20,
                'tax' => 182336,
                'total' => 1839936,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '4',
                'sale_date' => '2023-09-03',
                'discount' => 0,
                'tax' => 277101,
                'total' => 2796201,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '5',
                'sale_date' => '2023-09-04',
                'discount' => 0,
                'tax' => 149132,
                'total' => 1504882,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '6',
                'sale_date' => '2023-09-05',
                'discount' => 0,
                'tax' => 431046,
                'total' => 4349646,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '7',
                'sale_date' => '2023-09-05',
                'discount' => 0,
                'tax' => 254650,
                'total' => 2569650,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '2',
                'sale_date' => '2023-09-06',
                'discount' => 20,
                'tax' => 111760,
                'total' => 1127760,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '4',
                'sale_date' => '2023-09-06',
                'discount' => 0,
                'tax' => 98780,
                'total' => 996780,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '8',
                'sale_date' => '2023-09-07',
                'discount' => 0,
                'tax' => 64251,
                'total' => 648351,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '9',
                'sale_date' => '2023-09-08',
                'discount' => 0,
                'tax' => 118651,
                'total' => 1197301,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'customer_id' => '1',
                'sale_date' => '2023-09-08',
                'discount' => 0,
                'tax' => 64251,
                'total' => 648351,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
        Schema::enableForeignKeyConstraints();
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            LeadSeeder::class,
            CustomerSeeder::class,
            ProductSeeder::class,
            SalesMasterSeeder::class,
            SalesDetailSeeder::class,
            DeliverySeeder::class
        ]);
    }
}

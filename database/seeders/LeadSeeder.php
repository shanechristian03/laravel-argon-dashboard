<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('leads')->truncate();
        DB::table('leads')->insert([
            [
                'name' => 'Andy',
                'email' => 'andy@gmail.com',
                'phone' => '0843299442122',
                'company_name' => 'Melon',
                'company_address' => 'Fruit Street',
                'company_city' => 'Jakarta',
                'company_country' => 'Indonesia',
                'company_zip_code' => '74344',
                'company_website' => 'https://melon.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Cori',
                'email' => 'coriii@gmail.com',
                'phone' => '0877300452894',
                'company_name' => 'Great Company',
                'company_address' => 'New Way Street',
                'company_city' => 'Jambi',
                'company_country' => 'Indonesia',
                'company_zip_code' => '31644',
                'company_website' => 'https://greatc.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Suprianto',
                'email' => 'supri@gmail.com',
                'phone' => '081543999082',
                'company_name' => 'Lively',
                'company_address' => 'Test Street',
                'company_city' => 'Bandung',
                'company_country' => 'Indonesia',
                'company_zip_code' => '53566',
                'company_website' => 'https://www.lively.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Florentin',
                'email' => 'flower@gmail.com',
                'phone' => '0843299442122',
                'company_name' => 'Floristic',
                'company_address' => 'Flower Street',
                'company_city' => 'Jakarta',
                'company_country' => 'Indonesia',
                'company_zip_code' => '72235',
                'company_website' => 'https://floristic.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Barry',
                'email' => 'barry212@gmail.com',
                'phone' => '0458512732',
                'company_name' => 'Flashy',
                'company_address' => 'Cool Street',
                'company_city' => 'Canberra',
                'company_country' => 'Australia',
                'company_zip_code' => '28931',
                'company_website' => 'https://www.flashy.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Kevin',
                'email' => 'kev10@gmail.com',
                'phone' => '086440181752',
                'company_name' => null,
                'company_address' => null,
                'company_city' => null,
                'company_country' => null,
                'company_zip_code' => null,
                'company_website' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Mikey',
                'email' => 'mike77@gmail.com',
                'phone' => '0143716457',
                'company_name' => 'Durianis',
                'company_address' => 'Sweet Street',
                'company_city' => 'Kuala Lumpur',
                'company_country' => 'Malaysia',
                'company_zip_code' => '46312',
                'company_website' => 'https://durianis.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}

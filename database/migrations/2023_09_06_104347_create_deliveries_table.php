<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sale_id');
            $table->date('order_received_date')->nullable();
            $table->date('order_packing_date')->nullable();
            $table->date('start_delivering_date')->nullable();
            $table->date('product_received_date')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('country');
            $table->integer('distance');
            $table->timestamps();

            $table->foreign('sale_id')->references('id')->on('sales_master')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('deliveries');
    }
};
